package testing

import (
	"crypto/rand"
	"encoding/base64"
	mrand "math/rand"
	"os"
	"path/filepath"
	"runtime"
	"sync"
	"testing"
	"time"

	"git.openprivacy.ca/cwtch.im/tapir"
	"git.openprivacy.ca/cwtch.im/tapir/applications"
	"git.openprivacy.ca/cwtch.im/tapir/networks/tor"
	"git.openprivacy.ca/cwtch.im/tapir/primitives"
	ctor "git.openprivacy.ca/openprivacy/connectivity/tor"
	"git.openprivacy.ca/openprivacy/log"
	"golang.org/x/crypto/ed25519"
)

func TestTapirMaliciousRemote(t *testing.T) {

	numRoutinesStart := runtime.NumGoroutine()
	log.SetLevel(log.LevelDebug)
	log.Infof("Number of goroutines open at start: %d", runtime.NumGoroutine())
	// Connect to Tor
	os.Mkdir("tordir", 0700)
	dataDir := filepath.Join("tordir", "tor")
	os.MkdirAll(dataDir, 0700)

	// we don't need real randomness for the port, just to avoid a possible conflict...
	socksPort := mrand.Intn(1000) + 9051
	controlPort := mrand.Intn(1000) + 9052

	// generate a random password
	key := make([]byte, 64)
	_, err := rand.Read(key)
	if err != nil {
		panic(err)
	}

	useCache := os.Getenv("TORCACHE") == "true"

	torDataDir := ""
	if useCache {
		log.Infof("using tor cache")
		torDataDir = filepath.Join(dataDir, "data-dir-torcache")
		os.MkdirAll(torDataDir, 0700)
	} else {
		log.Infof("using clean tor data dir")
		if torDataDir, err = os.MkdirTemp(dataDir, "data-dir-"); err != nil {
			t.Fatalf("could not create data dir")
		}
	}

	ctor.NewTorrc().WithSocksPort(socksPort).WithOnionTrafficOnly().WithHashedPassword(base64.StdEncoding.EncodeToString(key)).WithControlPort(controlPort).Build("tordir/tor/torrc")
	acn, err := ctor.NewTorACNWithAuth("./tordir", filepath.Join("..", "tor"), torDataDir, controlPort, ctor.HashedPasswordAuthenticator{Password: base64.StdEncoding.EncodeToString(key)})
	if err != nil {
		t.Fatalf("Could not start Tor: %v", err)
	}
	log.Infof("Waiting for tor to bootstrap...")
	acn.WaitTillBootstrapped()
	defer acn.Close()
	t.Logf("Bootstrapped...")

	// Generate Server Keys, not we generate two sets
	id, _ := primitives.InitializeEphemeralIdentity()
	id2, sk2 := primitives.InitializeEphemeralIdentity()

	// Init the Server running the Simple App.
	service := new(tor.BaseOnionService)
	// Initialize an onion service with one identity, but the auth app with another, this should
	// trigger a failure in authentication protocol
	service.Init(acn, sk2, &id)

	// Goroutine Management
	sg := new(sync.WaitGroup)
	sg.Add(1)
	go func() {
		service.Listen(new(applications.AuthApp))
		sg.Done()
	}()

	// Wait for server to come online
	time.Sleep(time.Second * 60)
	wg := new(sync.WaitGroup)
	wg.Add(1)
	// Init a Client to Connect to the Server
	log.Infof("initializing the client....")
	client, _ := genclient(acn)
	go connectclientandfail(client, id2.PublicKey(), wg, t)
	wg.Wait()
	// Wait for Server to Sync
	time.Sleep(time.Second * 2)
	log.Infof("closing ACN...")
	client.Shutdown()
	service.Shutdown()
	acn.Close()
	sg.Wait()
	time.Sleep(time.Second * 5) // wait for goroutines to finish...
	log.Infof("Number of goroutines open at close: %d", runtime.NumGoroutine())
	if numRoutinesStart != runtime.NumGoroutine() {
		t.Errorf("Potential goroutine leak: Num Start:%v NumEnd: %v", numRoutinesStart, runtime.NumGoroutine())
	}
}

// Client will Connect and launch it's own Echo App goroutine.
func connectclientandfail(client tapir.Service, key ed25519.PublicKey, group *sync.WaitGroup, t *testing.T) {
	client.Connect(ctor.GetTorV3Hostname(key), new(applications.AuthApp))

	// Once connected, it shouldn't take long to authenticate and run the application. So for the purposes of this demo
	// we will wait a little while then exit.
	time.Sleep(time.Second * 5)

	log.Infof("Checking connection status...")
	conn, err := client.GetConnection(ctor.GetTorV3Hostname(key))
	if err == nil {
		group.Done()
		t.Errorf("Connection should have failed! %v %v", conn, err)
	}
	log.Infof("Successfully failed to authenticate...")
	group.Done()
}
