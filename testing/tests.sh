#!/bin/bash

set -e
pwd
go test -race ${1} -coverprofile=applications.cover.out -v ./applications
go test -race ${1} -coverprofile=applications.tokenboard.cover.out -v ./applications/tokenboard
# persistence is broken in WSL
if grep -q -v Microsoft /proc/version; then
	go test -race ${1} -coverprofile=persistence.cover.out -v ./persistence
fi
go test -race ${1} -coverprofile=primitives.cover.out -v ./primitives
go test -race ${1} -coverprofile=primitives.auditable.cover.out -v ./primitives/auditable
go test -race ${1} -coverprofile=primitives.core.cover.out -v ./primitives/core
# persistence is broken in WSL
if grep -q -v Microsoft /proc/version; then
	go test -race ${1} -coverprofile=primitives.privacypass.cover.out -v ./primitives/privacypass
	go test -bench "BenchmarkAuditableStore" -benchtime 1000x primitives/auditable/*.go
fi
echo "mode: set" > coverage.out && cat *.cover.out | grep -v mode: | sort -r | \
awk '{if($1 != last) {print $0;last=$1}}' >> coverage.out
rm -rf *.cover.out
