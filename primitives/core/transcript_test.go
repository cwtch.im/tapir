package core

import (
	"testing"
)

func TestNewTranscript(t *testing.T) {

	// Some very basic integrity checking
	transcript := NewTranscript("label")

	transcript.AddToTranscript("action", []byte("test data"))

	firstAudit := transcript.OutputTranscriptToAudit()
	secondAudit := transcript.OutputTranscriptToAudit()
	if firstAudit != secondAudit {
		t.Fatalf("Multiple Audit Calls should not impact underlying Transcript")
	}
	t.Logf("%v", transcript.OutputTranscriptToAudit())
	t.Logf("%v", transcript.CommitToTranscript("first commit"))
	t.Logf("%v", transcript.OutputTranscriptToAudit())
	t.Logf("%v", transcript.CommitToTranscript("second commit"))
	t.Logf("%v", transcript.OutputTranscriptToAudit())

	transcript.AddToTranscript("action", []byte("test data"))

	t.Logf("%v", transcript.CommitToTranscript("third commit"))
	t.Logf("%v", transcript.OutputTranscriptToAudit())

}
