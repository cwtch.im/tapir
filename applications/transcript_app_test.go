package applications

import "testing"

func TestTranscriptApp(t *testing.T) {
	ta := new(TranscriptApp)
	ta.Init(MockConnection{})
	ta.Transcript().NewProtocol("test")
	ta.transcript.CommitToTranscript("test-commit")
	t.Logf(ta.Transcript().OutputTranscriptToAudit())

	// Now we test panic'ing....
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic - it definitely should have")
		}
	}()

	// Attempt to reinitialized the transcript, apps should *never* do this and we want to be hostile to that
	// behaviour
	ta.Init(MockConnection{})
}
