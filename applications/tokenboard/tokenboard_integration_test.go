package tokenboard

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
	mrand "math/rand"
	"os"
	"path/filepath"
	"runtime"
	"sync"
	"testing"
	"time"

	"git.openprivacy.ca/cwtch.im/tapir/applications"
	"git.openprivacy.ca/cwtch.im/tapir/networks/tor"
	"git.openprivacy.ca/cwtch.im/tapir/primitives"
	"git.openprivacy.ca/cwtch.im/tapir/primitives/auditable"
	"git.openprivacy.ca/cwtch.im/tapir/primitives/privacypass"
	"git.openprivacy.ca/openprivacy/connectivity"
	ctor "git.openprivacy.ca/openprivacy/connectivity/tor"
	"git.openprivacy.ca/openprivacy/log"
)

type Handler struct {
	Store *auditable.Store
}

func (h Handler) HandleNewMessages(previousLastCommit []byte) {
	log.Debugf("Handling Messages After %x", previousLastCommit)
	messages := h.Store.GetMessagesAfter(previousLastCommit)
	for _, message := range messages {
		log.Debugf("Message %s", message)
	}
}

type FreePaymentHandler struct {
	tokens         []*privacypass.Token
	TokenService   *privacypass.TokenServer
	ACN            connectivity.ACN
	ServerHostname string
}

func (fph *FreePaymentHandler) MakePayment() {
	id, sk := primitives.InitializeEphemeralIdentity()
	client := new(tor.BaseOnionService)
	client.Init(fph.ACN, sk, &id)

	tokenApplication := new(applications.TokenApplication)
	tokenApplication.TokenService = fph.TokenService
	powTokenApp := new(applications.ApplicationChain).
		ChainApplication(new(applications.ProofOfWorkApplication), applications.SuccessfulProofOfWorkCapability).
		ChainApplication(tokenApplication, applications.HasTokensCapability)
	client.Connect(fph.ServerHostname, powTokenApp)
	conn, err := client.WaitForCapabilityOrClose(fph.ServerHostname, applications.HasTokensCapability)
	if err == nil {
		powtapp, _ := conn.App().(*applications.TokenApplication)
		fph.tokens = append(fph.tokens, powtapp.Tokens...)
		log.Debugf("Transcript: %v", powtapp.Transcript().OutputTranscriptToAudit())
		conn.Close()
		return
	}
	log.Debugf("Error making payment: %v", err)
}

func (fph *FreePaymentHandler) NextToken(data []byte, hostname string) (privacypass.SpentToken, error) {
	if len(fph.tokens) == 0 {
		return privacypass.SpentToken{}, errors.New("No more tokens")
	}
	token := fph.tokens[0]
	fph.tokens = fph.tokens[1:]
	return token.SpendToken(append(data, hostname...)), nil
}

func TestTokenBoardApp(t *testing.T) {
	//	numRoutinesStart := runtime.NumGoroutine()
	log.SetLevel(log.LevelDebug)
	log.Infof("Number of goroutines open at start: %d", runtime.NumGoroutine())

	os.Mkdir("tordir", 0700)
	dataDir := filepath.Join("tordir", "tor")
	os.MkdirAll(dataDir, 0700)

	// we don't need real randomness for the port, just to avoid a possible conflict...
	socksPort := mrand.Intn(1000) + 9051
	controlPort := mrand.Intn(1000) + 9052

	// generate a random password
	key := make([]byte, 64)
	_, err := rand.Read(key)
	if err != nil {
		panic(err)
	}

	useCache := os.Getenv("TORCACHE") == "true"

	torDataDir := ""
	if useCache {
		log.Infof("using tor cache")
		torDataDir = filepath.Join(dataDir, "data-dir-torcache")
		os.MkdirAll(torDataDir, 0700)
	} else {
		log.Infof("using clean tor data dir")
		if torDataDir, err = os.MkdirTemp(dataDir, "data-dir-"); err != nil {
			t.Fatalf("could not create data dir")
		}
	}

	ctor.NewTorrc().WithSocksPort(socksPort).WithOnionTrafficOnly().WithHashedPassword(base64.StdEncoding.EncodeToString(key)).WithControlPort(controlPort).Build("tordir/tor/torrc")
	acn, err := ctor.NewTorACNWithAuth("./tordir", filepath.Join("..", "tor"), torDataDir, controlPort, ctor.HashedPasswordAuthenticator{Password: base64.StdEncoding.EncodeToString(key)})
	if err != nil {
		t.Fatalf("Could not start Tor: %v", err)
	}
	log.Infof("Waiting for tor to bootstrap...")
	acn.WaitTillBootstrapped()
	defer acn.Close()
	t.Logf("Bootstrapped...")
	// Generate Server Key
	sid, sk := primitives.InitializeEphemeralIdentity()
	tokenService := privacypass.NewTokenServer()
	serverAuditableStore := new(auditable.Store)
	serverAuditableStore.Init(sid)

	clientAuditableStore := new(auditable.Store)
	// Only initialize with public parameters
	sidpubk := sid.PublicKey()
	publicsid := primitives.InitializeIdentity("server", nil, &sidpubk)
	clientAuditableStore.Init(publicsid)

	// Init the Server running the Simple App.
	service := new(tor.BaseOnionService)
	service.Init(acn, sk, &sid)

	// Goroutine Management
	sg := new(sync.WaitGroup)
	sg.Add(1)
	go func() {
		service.Listen(NewTokenBoardServer(tokenService, serverAuditableStore))
		sg.Done()
	}()

	// Init the Server running the PoW Token App.
	powTokenService := new(tor.BaseOnionService)
	spowid, spowk := primitives.InitializeEphemeralIdentity()
	powTokenService.Init(acn, spowk, &spowid)
	sg.Add(1)
	go func() {
		tokenApplication := new(applications.TokenApplication)
		tokenApplication.TokenService = tokenService
		powTokenApp := new(applications.ApplicationChain).
			ChainApplication(new(applications.ProofOfWorkApplication), applications.SuccessfulProofOfWorkCapability).
			ChainApplication(tokenApplication, applications.HasTokensCapability)
		powTokenService.Listen(powTokenApp)
		sg.Done()
	}()

	time.Sleep(time.Second * 60) // wait for server to initialize
	id, sk := primitives.InitializeEphemeralIdentity()
	client := new(tor.BaseOnionService)
	client.Init(acn, sk, &id)
	client.Connect(sid.Hostname(), NewTokenBoardClient(clientAuditableStore, Handler{Store: clientAuditableStore}, &FreePaymentHandler{ACN: acn, TokenService: tokenService, ServerHostname: spowid.Hostname()}))
	client.WaitForCapabilityOrClose(sid.Hostname(), applications.AuthCapability)
	conn, _ := client.GetConnection(sid.Hostname())
	tba, _ := conn.App().(*Client)
	tba.PurchaseTokens()
	tba.Post([]byte("HELLO 1"))
	tba.Post([]byte("HELLO 2"))
	tba.Post([]byte("HELLO 3"))
	tba.Post([]byte("HELLO 4"))
	tba.Post([]byte("HELLO 5"))
	tba.Replay()
	time.Sleep(time.Second * 10) // We have to wait for the async replay request!
	tba.Post([]byte("HELLO 6"))
	tba.Post([]byte("HELLO 7"))
	tba.Post([]byte("HELLO 8"))
	tba.Post([]byte("HELLO 9"))
	tba.Post([]byte("HELLO 10"))
	tba.Replay()
	time.Sleep(time.Second * 10) // We have to wait for the async replay request!

	if tba.Post([]byte("HELLO 11")) {
		t.Errorf("Post should have failed.")
	}

	time.Sleep(time.Second * 10)
	acn.Close()
	sg.Wait()
}
